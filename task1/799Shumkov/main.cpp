#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <glm/gtx/string_cast.hpp>
#include <glm/vec3.hpp>
#include <glm/gtx/vector_angle.hpp>

#include <iostream>
#include <vector>
#include <stack>


class Cone {
public:
    explicit Cone(float radius, float height, size_t frag):
        radius_(radius),
        height_(height),
        frag_(frag),
        center_(glm::vec3(0, 0, 0)),
        h_vec_(glm::vec3(0, 0, 1)),
        top_(glm::vec3(0, 0, height_))
    {
        float hypot = sqrt(height * height + radius * radius);
        float phi = 2 * glm::pi<float>() / frag_;
        auto cur_p = glm::vec3(radius, 0, 0);
        auto cur_norm = glm::vec3(height_ / hypot, 0, radius_ / hypot);
        for (size_t i = 0; i < frag_ - 1; i++) {
            points_.push_back(cur_p);
            cur_p = glm::rotate(cur_p, phi, glm::vec3(0, 0, 1));
            normals_.push_back(cur_norm);
            cur_norm = glm::rotate(cur_norm, phi, glm::vec3(0, 0, 1));
        }
        points_.push_back(cur_p);
        normals_.push_back(cur_norm);
    }

    void rotate(glm::vec3 axis, float phi) {
        for (size_t i = 0; i < points_.size(); i++) {
            points_[i] = center_ + glm::rotate(points_[i] - center_, phi, axis);
        }
        for (size_t i = 0; i < normals_.size(); i++) {
            normals_[i] = glm::rotate(normals_[i], phi, axis);
        }
        h_vec_ = glm::rotate(h_vec_, phi, axis);
        top_ = center_ + glm::rotate(top_ - center_, phi, axis);
    }


    void transform(float coef_r, float coef_h) {
        Cone temp(radius_ * coef_r, height_ * coef_h, frag_);
        glm::vec3 shift = center_ + height_ * h_vec_;
        for (int i = 0; i < points_.size(); i++) {
            temp.points_[i] += shift;
        }
        temp.center_ += shift;
        temp.top_ += shift;
        auto axis = glm::cross(h_vec_, temp.h_vec_);
        if (length(axis) == 0) {
            *this = temp;
            return;
        }
        axis = glm::normalize(axis);
        temp.rotate(axis, glm::orientedAngle(temp.h_vec_, h_vec_, axis));
        *this = temp;
    }
    std::vector<float> get_points() const {
        std::vector<float> res;

        for (size_t i = 0; i < points_.size(); i++) {
            res.push_back(points_[i].x);
            res.push_back(points_[i].y);
            res.push_back(points_[i].z);
            size_t j = (i + 1) % points_.size();
            res.push_back(points_[j].x);
            res.push_back(points_[j].y);
            res.push_back(points_[j].z);
            res.push_back(top_.x);
            res.push_back(top_.y);
            res.push_back(top_.z);
        }
        return res;
    }

    std::vector<float> get_normals() const {
        std::vector<float> res;
        float phi = 2 * glm::pi<float>() / frag_;
        for (size_t i = 0; i < points_.size(); i++) {
            res.push_back(normals_[i].x);
            res.push_back(normals_[i].y);
            res.push_back(normals_[i].z);
            size_t j = (i + 1) % points_.size();
            res.push_back(normals_[j].x);
            res.push_back(normals_[j].y);
            res.push_back(normals_[j].z);
            auto top_normal = glm::rotate(normals_[i], phi / 2, glm::vec3(0, 0, 1));
            res.push_back(top_normal.x);
            res.push_back(top_normal.y);
            res.push_back(top_normal.z);
        }
        return res;
    }

    size_t size() const {
        return points_.size();
    }

private:
    float radius_;
    float height_;
    size_t frag_;

    glm::vec3 center_;
    glm::vec3 h_vec_;
    glm::vec3 top_;

    std::vector<glm::vec3> points_;
    std::vector<glm::vec3> normals_;
};

enum class LState {
    FORWARD,
    ROTATION1,
    ROTATION2,
    ROTATION3,
    ROTATION4,
    PUSH,
    POP
};

std::vector<LState> next(const std::vector<LState>& chain) {
    std::vector<LState> res;
    for (auto cur : chain) {
        if (cur == LState::FORWARD) {
            res.push_back(LState::FORWARD);
            res.push_back(LState::PUSH);
            res.push_back(LState::ROTATION1);
            res.push_back(LState::FORWARD);
            res.push_back(LState::POP);
            res.push_back(LState::PUSH);
            res.push_back(LState::ROTATION2);
            res.push_back(LState::FORWARD);
            res.push_back(LState::POP);
            res.push_back(LState::PUSH);
            res.push_back(LState::ROTATION3);
            res.push_back(LState::FORWARD);
            res.push_back(LState::POP);
            res.push_back(LState::PUSH);
            res.push_back(LState::ROTATION4);
            res.push_back(LState::FORWARD);
            res.push_back(LState::POP);
        } else {
            res.push_back(cur);
        }
    }
    return res;
}


std::vector<Cone> get_branches(const std::vector<LState>& chain) {
    std::stack<Cone> stack;
    std::vector<Cone> res;
    Cone current(0.1, 1, 20);
    float coef_r = 0.6;
    float coef_h = 0.8;
    for (auto cur : chain) {
        switch (cur) {
            case LState::FORWARD : {
                res.push_back(current);
                current.transform(coef_r, coef_h);
            } break;
            case LState::ROTATION1 : {
                current.rotate(glm::vec3(1, 0, 0), glm::pi<float>() / 4);
            } break;
            case LState::ROTATION2 : {
                current.rotate(glm::vec3(1, 0, 0), -glm::pi<float>() / 9);
            } break;
            case LState::ROTATION3 : {
                current.rotate(glm::vec3(0, 1, 0), glm::pi<float>() / 4);
            } break;
            case LState::ROTATION4 : {
                current.rotate(glm::vec3(0, 1, 0), -glm::pi<float>() / 4);
            } break;
            case LState::PUSH : {
                stack.push(current);
            } break;
            case LState::POP : {
                current = stack.top();
                stack.pop();
            } break;
        }
    }
    return res;
}


class Tree {
public:
    explicit Tree (std::vector<Cone> cones): cones_(std::move(cones)) {};
    std::vector<float> get_points() const {
        std::vector<float> res;
        for (auto cone : cones_) {
            std::vector<float> cur_points = cone.get_points();
            for (auto point : cur_points) {
                res.push_back(point);
            }
        }
        return res;
    }
    std::vector<float> get_normals() const {
        std::vector<float> res;
        for (auto cone : cones_) {
            std::vector<float> cur_normals = cone.get_points();
            for (auto point : cur_normals) {
                res.push_back(point);
            }
        }
        return res;
    }
private:
    std::vector<Cone> cones_;
};


class TreeApplication : public Application {
public:
    void makeScene() override {
        Application::makeScene();

        std::vector<LState> chain = {LState::FORWARD};
        size_t cnt_step = 5;
        for (size_t i = 0; i < cnt_step; i++) {
            chain = next(chain);
        }
        Tree tree(get_branches(chain));

        std::vector<float> points = tree.get_points();
        DataBufferPtr points_buf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        points_buf->setData(points.size() * sizeof(float), points.data());

        tree_mesh_ = std::make_shared<Mesh>();
        tree_mesh_->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), 0, points_buf);
        tree_mesh_->setPrimitiveType(GL_TRIANGLES);
        tree_mesh_->setVertexCount(points.size() / 3);

        std::vector<float> normals = tree.get_normals();
        DataBufferPtr normals_buf = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
        normals_buf->setData(normals.size() * sizeof(float), normals.data());

        tree_mesh_->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 3*sizeof(float), 0, normals_buf);
        tree_mesh_->setModelMatrix(glm::mat4(1.0));
        _cameraMover = std::make_shared<FreeCameraMover>();

        shader_ = std::make_shared<ShaderProgram>("799ShumkovData1/shader.vert", "799ShumkovData1/shader.frag");

    }
    void draw() override {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        shader_->use();
        shader_->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader_->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        shader_->setMat4Uniform("modelMatrix", tree_mesh_->modelMatrix());

        tree_mesh_->draw();
    }

private:
    ShaderProgramPtr shader_;
    MeshPtr tree_mesh_;
};


int main() {
    TreeApplication app;
    app.start();
}
