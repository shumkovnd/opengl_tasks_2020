set(SRC_FILES
        main.cpp
        common/Application.cpp
        common/Camera.cpp
        common/ConditionalRender.cpp
        common/DebugOutput.cpp
        common/Framebuffer.cpp
        common/Mesh.cpp
        common/QueryObject.cpp
        common/ShaderProgram.cpp
        common/SkinnedMesh.cpp
        common/Texture.cpp
        )

add_compile_definitions(GLM_ENABLE_EXPERIMENTAL)
include_directories(common)
MAKE_OPENGL_TASK(799Shumkov 1 "${SRC_FILES}")
if (UNIX)
    target_link_libraries(799Shumkov1 stdc++fs)
endif()